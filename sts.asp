<%@  language="javascript" %>
    <%
        // Simple Table Service 
        // by Travis Leithead
        // May 2013

        // Types
        var INT = 7; // chars
        var FLAG = 1; // chars
        var FILE_EXTENSION = ".txt"; // simple table service
        var COLSPLITTER = "@@@";
        var MASTER_TABLE_NAME = "MASTER";

        // the Table
        // Initialize with a name (unique), and column info in the form:
        // [
        //    { name: "colname", size: 10 },
        //    { name: "colname2", size: 5 }
        // ]
        // Throws exceptions if columnInfoArray structure does not validate.
        function Table(uniqueName, columnInfoArray) {
            this.NAME = uniqueName;
            this.COLS = columnInfoArray;
            // Column validation and common data caching...
            if ((typeof columnInfoArray != "object") || (columnInfoArray.length < 1))
                throw Error("The columnInfoArray parameter must be an array with at least one column structure");
            var colUniqueness = {};
            var colSizeTotal = 0;
            for (var i = 0, len = columnInfoArray.length; i < len; i++) {
                var colOb = columnInfoArray[i];
                if ((typeof colOb.name != "string") || (typeof colOb.size != "number"))
                    throw Error("columnInfoArray's elements must be objects with a name (string) size (number) properties for the column info");
                if (colUniqueness[colOb.name])
                    throw Error("Column names must be unique: \"" + colOb.name + "\" used at least twice");
                colUniqueness[colOb.name] = true;
                if (colOb.size < 1)
                    throw Error("Column size cannot be less than one character");
                colSizeTotal += colOb.size;
            }
            this.ROWSIZE = INT + // continuation row index
                           FLAG + // row status/identifier
                          (INT * columnInfoArray.length) + // content size field for each column
                           colSizeTotal;
        }

        // General Flat-file database on-disk layout:
        // Raw index: 0
        // +---------------+
        // | Start of next |
        // | available row |
        // | index (INT)   |
        // +---------------+
        // Row index: 0
        // +-----------+-------+----------------+--------------+----------------+--------------+------+
        // | Continue  |  Row  |                |              |                |              |      |
        // | Row Index | State | Header Content | Header Data  | Header Content | Header Data  |      |
        // | (INT)     | (FLAG)| Length (INT)   | (FIXED-SIZE) | Length (INT)   | (FIXED-SIZE) | etc. |
        // +-----------+-------+----------------+--------------+----------------+--------------+------+
        // Row index: 1
        // [...]
        //
        // * Continue Row Index - if the row does not continue, then this will be zero. 
        //   Otherwise, it's the row index where the data for this row continues. When 
        //   iterating the table, rows with a non-zero Continue Row Index are not unique 
        //   row entries in the table. NOTE: Row indexes are absolute and non-contiguous; i.e.,
        //   Another database of virtual row index to literal row index is necessary for 
        //   random access to this table structure.
        // * Row State - a flag indicating what the current row represents. The following
        //   flags are defined:
        //   - 0 = Row represents free space (unused unique index) (occurs when rows are deleted
        //         or when a row with a continuation is filled with a shorter row.
        //   - 1 = (Start of) a unique row. May have continuations
        //   - 2 = Continuation data (allocated/not free)
        // * Header Content Length - this value reports the exact length of the content in the
        //   next column (since header data is always padded with spaces, it's important to 
        //   know when the actual stored data (which may include trailing spaces) ends. This
        //   number will not exceed the length of the next column's fixed-size content (it
        //   is a local value for each row in a series of continuations).
        // * Header Data - the content of the row.
        // When a row is cached in the Application object, all continuations will be resolved, 
        // no content-length values are stored, and header-data is separated by the special
        // token COLSPLITTER (e.g., "@@@" which is the only value not allowed on data insertion 
        // in order to simplify the design vs. extra escaping logic). When the data is returned from the
        // table, however, it is split out into an array with each column's data in the 
        // corresponding column index's values.


        // Reads a single table row (including continuations) from the table.
        // If the rowIndex is not valid, e.g., it is a continuation row, then this API 
        // throws. Note this method closes the file on exit; do not use it for
        // iterating the file (see cacheAllRows instead).
        // Returned row format is { rowId: [row, array] } (same format as the readAllRows method)
        Table.prototype.readRow = function (rowIndex) {
            // Return a value from the cache if available--otherwise pull it from disk.
            var cacheResult = Application(this.NAME + rowIndex);
            var cacheResultContainer = {};
            if (typeof cacheResult == "string") {
                cacheResultContainer[rowIndex] = cacheResult.split(COLSPLITTER);
                return cacheResultContainer;
            }
            // Do as much work as I can before taking the lock...
            // Skip to the specific row...
            var currentRow = rowIndex * this.ROWSIZE;
            // Protect (and serialize) disk I/O to avoid file corruption
            Application.Lock();
            var newTextStream = _openFlatTextFile(this.NAME, false);
            if (!newTextStream) {
                Application.Unlock();
                throw Error("Table " + this.NAME + " has no such row (persisted table not found)");
            }
            var LASTROW = parseInt(newTextStream.Read(INT));
            // Validate that rowIndex isn't out-of-bounds
            if ((rowIndex >= LASTROW) || (rowIndex < 0)) {
                newTextStream.Close();
                Application.Unlock();
                throw Error("Requested row " + rowIndex + " does not exist in table " + this.NAME + " (row index out of bounds)");
            }
            newTextStream.Skip(currentRow);
            var colValues = [];
            var firstPass = true;
            do {
                var continueIndex = parseInt(newTextStream.Read(INT));
                var rowType = parseInt(newTextStream.Read(FLAG));
                if (firstPass && ((rowType == 2) || (rowType == 0))) { // If the first row is free-space or a continuation, then this is a bad index request!
                    newTextStream.Close();
                    Application.Unlock();
                    throw Error("Requested row " + rowIndex + " does not exist in table " + this.NAME + " (row index " + ((rowType == 2) ? "overlaps existing data)" : "is unallocated)"));
                }
                firstPass = false;
                for (var j = 0; j < this.COLS.length; j++) {
                    var columnDataLength = parseInt(newTextStream.Read(INT));
                    if (!colValues[j])
                        colValues.push("");
                    colValues[j] += newTextStream.Read(this.COLS[j].size).substring(0, columnDataLength);
                }
                if (continueIndex != 0) {
                    newTextStream.Skip((continueIndex - currentRow - 1) * this.ROWSIZE); // The '1' is because I just read the entire currentRow...
                    currentRow = continueIndex;
                }
            } while (continueIndex != 0);
            newTextStream.Close();
            // Cache the key
            var packedString = colValues.join(COLSPLITTER);
            Application(this.NAME + rowIndex) = packedString;
            Application.Unlock();
            cacheResultContainer[rowIndex] = colValues;
            return cacheResultContainer;
        }


        // The algorithm to return all rows is optimized to avoid needing to seek
        // backwards even when continuations are found. This method avoids open/close thrashing
        // on the text file (which would happen via cacheSingleRow). Returns true on success,
        // false on failure to cache (no file to read).
        // When done, this also adds some meta information into the cache:
        // * [prefix]__allcached - boolean flag indicating that this caching has been done and all rows are in the cache
        // * [prefix]__totalrows - number of unique row items in the table (excludes continuations, empty rows)
        // * [prefix]__rawslots - number of physical rows on disk (includes all free rows, etc.)
        // * [prefix]__rawholes - number of physical rows that are free/unallocated space (file fragmentation)
        // This method does not throw exceptions.
        Table.prototype.readAllRows = function () {
            // final cached rows
            var allRows = {};
            if (Application(this.NAME + "__allcached")) {
                var targetRowCount = Application(this.NAME + "__totalrows");
                var tablePattern = new RegExp("^" + this.NAME); // Match the beginning of the string starting with NAME...
                var exclusionPattern = new RegExp("(__allcached|__totalrows|__rawslots|__rawholes)");
                var offset = this.NAME.length;
                // Here's some IE-only weird (but required) enumeration logic for ASP Collections (for..in doesn't work by default???)
                for (var appIterator = new Enumerator(Application.Contents), i = 0; !appIterator.atEnd() && (i < targetRowCount); appIterator.moveNext()) {
                    var key = appIterator.item();
                    // I must search the whole Application object for there is no other segregation of objects possible that persists across sessions...
                    if (tablePattern.test(key)) { // This key belongs to this table...
                        var uniqueRowId = key.substr(offset);
                        if (!exclusionPattern.test(uniqueRowId)) { // As long as the key is not one of the exclusions (table meta info)
                            allRows[uniqueRowId] = Application(key).split(COLSPLITTER); // Attach the split value to the unique id (stripped of table identifying info)
                            i++;
                        }
                    }
                }
                return allRows;
            }
            // Protect (and serialize) disk I/O to avoid file corruption
            Application.Lock();
            var newTextStream = _openFlatTextFile(this.NAME, false);
            if (!newTextStream) { // In the case where the backing data doesn't exist, then instead of throwing an exception...
                Application.Unlock();
                return allRows; // ...just report that no rows exist. Writes will auto-create the backing data, so no error is necessary. (specific row reads will still fail though)
            }
            var LASTROW = parseInt(newTextStream.Read(INT));
            var rowRemainderSize = this.ROWSIZE - INT - FLAG; // For skipping after reading the continuation and flag
            // Keep track of pending completions so that they can be back-filled as they are
            // encountered later in the file.
            var pendingCompletions = []; // Structure is keyed by next row index and has an object: { originalRowIndex: X, cols: [ value,... ] }
            // How many holes?
            var holeCount = 0;
            // Start iterating the rows of the text file...
            for (var i = 0; i < LASTROW; i++) {
                // Read continuation field
                var continues = parseInt(newTextStream.Read(INT));
                var rowState = parseInt(newTextStream.Read(FLAG));
                if (rowState == 0) { // Free-space row...
                    newTextStream.Skip(rowRemainderSize);
                    holeCount++;
                    continue; // Goto the next row...
                }
                // Use the previous column data if this row is a pending completion...
                var colContent = pendingCompletions[i] ? pendingCompletions[i].cols : [];
                // Read the columns...
                for (var j = 0; j < this.COLS.length; j++) {
                    var columnDataLength = parseInt(newTextStream.Read(INT));
                    if (!colContent[j])
                        colContent.push("");
                    colContent[j] += newTextStream.Read(this.COLS[j].size).substring(0, columnDataLength);
                }
                if (pendingCompletions[i]) { // This is a completion row...
                    if (continues != 0) // This continuation continues!
                        pendingCompletions[continues] = pendingCompletions[i]; // Set the new future row reference, and I'm done.
                    else // Finalize the original row in the allRows dictionary.
                        allRows[pendingCompletions[i].originalRowIndex] = colContent;
                }
                else { // The start of a new row...(can't be a continuation row (not yet seen) because continuations always FOLLOW their start row (top-down progression in the file)
                    if (continues != 0) // this new row but it continues
                        pendingCompletions[continues] = { originalRowIndex: i, cols: colContent };
                    else // Single row, doesn't continue.
                        allRows[i] = colContent; // Finalized.
                }
            }
            newTextStream.Close();
            var totalRows = 0;
            // Cache all the keys
            for (key in allRows) {
                Application(this.NAME + key) = allRows[key].join(COLSPLITTER);
                totalRows++;
            }
            // Cache the meta-data (the other functions that write/delete the database update these fields...
            Application(this.NAME + "__allcached") = true;
            Application(this.NAME + "__totalrows") = totalRows;
            Application(this.NAME + "__rawslots") = LASTROW;
            Application(this.NAME + "__rawholes") = holeCount;
            Application.Unlock();
            return allRows;
        }

        // Writes the given row to the table at the rowIndex position
        // or at the next available spot in the table if rowIndex is 
        // less than zero (the next available spot may be at the end--but 
        // the algorithm avoids this if possible to keep the file compact
        // and to fill any holes that develop).
        // If the data exceeds a given column size, the data is 
        // written to multiple rows in a continuation. Existing
        // continuations are re-used if possible. 
        // Throws an error if there was a failure to write the row. Otherwise, 
        // returns the unique rowIndex (starting row index in the case of a 
        // continuation) of the newly written row (0 or greater). If the 
        // table does not yet exist, it will be created and the row inserted.
        // Returned row indexes are guaranteed to be unique within the table,
        // but may be reclaimed and re-used if existing rows are deleted/overwritten.
        // The newRowArray should be an array where the number of elements match
        // the number of columns in the table
        // Mismatched rowData to the specified table layout throws exceptions.
        Table.prototype.writeRow = function(rowIndex, newRowArray) {
            // Prep the incoming data...
            if (!newRowArray || !newRowArray.length || (newRowArray.length != this.COLS.length))
                throw Error("Provided row data array of length " + newRow.length+ " does not match the table " + this.NAME + " column count (" + this.COLS.length + ")");
            // Convert the entities in the array to strings (if not already)
            for (var i = 0; i < newRowArray.length; i++)
                newRowArray[i] = newRowArray[i].toString();
            var newRows = [newRowArray.slice(0)]; // Wrap clone of original in an array (don't modify original, it's used later on.
            var nextContinuation = [];
            var needContinuation = true;
            // Ensure that all the column's data "fit" into each columns size limits...and build continuations as needed.
            for (var i = 0; needContinuation; i++) {
                needContinuation = false;
                nextContinuation = [];
                for (var c = 0; c < this.COLS.length; c++) {
                    if (newRows[i][c].length > this.COLS[c].size) {
                        needContinuation = true;
                        nextContinuation.push(newRows[i][c].slice(this.COLS[c].size)); // From size to the end goes in the next spot.
                        newRows[i][c] = newRows[i][c].slice(0, this.COLS[c].size); // Trim the original value's slot.
                    }
                    else
                        nextContinuation.push("");
                }
                if (needContinuation)
                    newRows.push(nextContinuation);
            }
            var file = null;
            var LASTROW = null;
            // Synchronize any file I/O
            Application.Lock();
            // Do a full read pass (since read/write file access is not available in basic ASP )
            var readStream = _openFlatTextFile(this.NAME, false);
            if (!readStream) { // No problem, create a new file in this case.
                file = "0".pad(INT);
                LASTROW = 0;
            }
            else { // Existing file
                file = readStream.ReadAll();
                readStream.Close();
                LASTROW = parseInt(file.substr(0, INT));
            }
            if (rowIndex >= LASTROW) {
                Application.Unlock();
                throw Error("Requested row " + rowIndex + " does not exist in table " + this.NAME + " (row index out of bounds)");
            }
            var currentLastRow = LASTROW;
            var currRowIndex = rowIndex;
            var nextRowIndex = -1;
            var lastRowIndex = -1;
            var firstRowIndex = null;
            // For tracking hole-changes to the file...
            var metastate_emptyRowsDelta = 0; // positive are an increased number of empty rows, negative value is a decreased number of empty rows
            // For tracking whether this an insertion or a replacement?
            var metastate_replaced = false;
            // Add each new row to the file at the appropriate location...
            for (var i = 0, len = newRows.length; i < len; i++) {
                // First ensure there's a concrete place to put the current row...
                if (currRowIndex < 0) { // Don't know yet where to put the current item...?
                    var searchRowOffset = 0; // Start from the beginning of the file unless...
                    if (lastRowIndex >= 0)
                        searchRowOffset = lastRowIndex + 1;
                    currRowIndex = _searchEmptyRow(file, this.ROWSIZE, searchRowOffset, ((i + 1) < len));
                    if (currRowIndex < 0) { // Still no home?
                        currRowIndex = currentLastRow;
                        currentLastRow++;
                        // Check for overflow in this index
                        if (currentLastRow.toString().length > INT) {
                            Application.Unlock();
                            throw Error("Cannot write the row to table " + this.NAME + " (addressable row index limit exceeded)");
                        }
                    }
                }
                // The currentRow is now converted to an actual index position now...
                // Get the existing rowState if applicable
                if (currRowIndex < LASTROW) { // An in-file replacement will occur
                    var rowState = parseInt(file.substr(INT + (currRowIndex * this.ROWSIZE) + INT, FLAG));
                    // Perform some extra first-time validation and state changes in this in-file insertion...
                    if (i == 0) {
                        if (rowState == 2) { // Trying to write to the data segment of a row is wrong and will lead to file corruption! Fail out.
                            Application.Unlock();
                            throw Error("Invalid row index for write to table " + this.NAME + " (row index overlaps existing table data)");
                        }
                        else if (rowState == 1) // Replacing an existing head entry.
                            metastate_replaced = true;
                    }
                    // Generally track the empty row removal...
                    if (rowState == 0)
                        metastate_emptyRowsDelta--; // One-less empty row
                }
                // Follow-up first time state (not dependent on being a row inside the file structure)
                if (i == 0)
                    firstRowIndex = currRowIndex; // Save this index for the end...
                // Figure out next continuation index (if applicable)
                var continuationIndex = 0; // Assume zero unless...
                if ((i + 1) < len) {
                    nextRowIndex = -1; // Assume I don't know yet what it will be...(it's OK for it to stay this way for the next loop)
                    if (currRowIndex < LASTROW) { // For a row that will replace an existing row in the file (two cases:)
                        var existingContinuation = parseInt(file.substr(INT + (currRowIndex * this.ROWSIZE), INT));
                        if (existingContinuation > 0) { // A pre-existing continuation is available: use it!
                            continuationIndex = existingContinuation;
                            nextRowIndex = existingContinuation;
                        }
                        else { // Existing slot did not continue...
                            // Find the next suitable place to continue this data...
                            nextRowIndex = _searchEmptyRow(file, this.ROWSIZE, currRowIndex + 1, ((i + 2) < len));
                            if (nextRowIndex >= 0) // Found a suitable spot to fill in the file already!
                                continuationIndex = nextRowIndex;
                            // else case is handled in the next outer if-block
                        }
                    }
                    if (nextRowIndex < 0) // still don't know where to go next--but need a continuation value...
                        continuationIndex = currentLastRow; // If I picked currentIndex to be the last row, I already incremented it above...
                }
                // Need to free leftover continuations that I may be overwriting under these conditions: 
                // 1. Last row to insert, 2. within the former file area, 3. row to replace has a continuation, 4. row to replace is not already freed
                if ((i == (len - 1)) && (currRowIndex < LASTROW)) {
                    var deleteRowIndex = parseInt(file.substr(INT + (currRowIndex * this.ROWSIZE), INT));
                    if (deleteRowIndex > 0) {
                        var freeResults = _freeRows(file, this.ROWSIZE, deleteRowIndex); // Returns: { newFile: "", numFreed: # }
                        file = freeResults.newFile;
                        metastate_emptyRowsDelta += freeResults.numFreed;
                    }
                }
                // Write the row...
                if ((currRowIndex.toString().length > INT) || (continuationIndex.toString().length > INT)) {
                    Application.Unlock();
                    throw Error("Cannot write the row to table " + this.NAME + " (addressable row continuation index limit exceeded)");
                }
                // Gather the columns together into the table layout format
                var rowColLayout = "";
                for (var r = 0, rLen = newRows[i].length; r < rLen; r++)
                    rowColLayout += newRows[i][r].length.pad(INT) + newRows[i][r].pad(this.COLS[r].size);
                file = file.overwrite(continuationIndex.pad(INT) + ((lastRowIndex < 0) ? 1 : 2).pad(FLAG) + rowColLayout,
                                      INT + (currRowIndex * this.ROWSIZE));
                lastRowIndex = currRowIndex;
                currRowIndex = nextRowIndex;
            }
            // Update the last row state if it changed
            if (currentLastRow != LASTROW) {
                file = file.overwrite(currentLastRow.pad(INT), 0);
                // Keep the meta information in sync if already present...
                if (typeof Application(this.NAME + "__rawslots") == "number")
                    Application(this.NAME + "__rawslots") = currentLastRow;
            }
            // Overwrite the old file with these updates...
            var writeStream = _openFlatTextFile(this.NAME, true); // Open for writing...
            writeStream.Write(file);
            writeStream.Close();
            // Update the cache too
            Application(this.NAME + firstRowIndex) = newRowArray.join(COLSPLITTER);
            // Update additional table meta info (if they are set) -- rawslots was updated just above...
            var totRows = Application(this.NAME + "__totalrows");
            if (!metastate_replaced && (typeof totRows == "number"))
                Application(this.NAME + "__totalrows") = totRows + 1;
            var totHoles = Application(this.NAME + "__rawholes");
            if (typeof totHoles == "number")
                Application(this.NAME + "__rawholes") = totHoles + metastate_emptyRowsDelta;
            Application.Unlock();
            return firstRowIndex;
        }

        
        // Marks a given row as free (but does not zero it out; allowing
        // it's existing continuation chain to be re-used later for other
        // new rows with continuations). Clears the row and it's continuations
        // (if any). Removes the existing entry from the cache as well.
        // Throws if there was a problem deleting the row, otherwise
        // returns the row index that was deleted.
        Table.prototype.deleteRow = function(rowIndex) {
            // Protect (and serialize) disk I/O to avoid file corruption
            Application.Lock();
            var newTextStream = _openFlatTextFile(this.NAME, false);
            if (!newTextStream) {
                Application.Unlock();
                throw Error("Table " + this.NAME + " has no such row (persisted table not found)");
            }
            var file = newTextStream.ReadAll();
            newTextStream.Close();
            if (rowIndex >= parseInt(file.substr(0, INT))) {
                Application.Unlock();
                throw Error("Requested row " + rowIndex + " does not exist in table " + this.NAME + " (row index out of bounds)");
            }
            var rowIndexType = parseInt(file.substr(INT + (rowIndex * this.ROWSIZE) + INT, FLAG));
            if ((rowIndexType == 2) || (rowIndexType == 0)) {
                Application.Unlock();
                throw Error("Requested row " + rowIndex + " does not exist in table " + this.NAME + " (row index " + ((rowIndexType == 2) ? "overlaps existing data)" : "is unallocated)"));
            }
            var freeResults = _freeRows(file, this.ROWSIZE, rowIndex); // Returns: { newFile: [""], numFreed: [#] }
            file = freeResults.newFile;
            // Overwrite the old file with these updates...
            var writeStream = _openFlatTextFile(this.NAME, true); // Open for writing...
            writeStream.Write(file);
            writeStream.Close();

            var totRows = Application(this.NAME + "__totalrows");
            if (typeof totRows == "number")
                Application(this.NAME + "__totalrows") = totRows - 1;
            var totHoles = Application(this.NAME + "__rawholes");
            if (typeof totHoles == "number")
                Application(this.NAME + "__rawholes") = totHoles + freeResults.numFreed;
            // the __rawslots rows will always be unchanged since deletion doesn't reduce the database file size.
            // Update the cache too
            Application.Contents.Remove(this.NAME + rowIndex);
            Application.Unlock();
            return rowIndex;
        }


        // Clears out any in-memory keys associated with this table, and 
        // deletes the on-disk file as well.
        Table.prototype.deleteAllRows = function () {
            // Clear all (any) rows from this table that are cached in app memory...
            var targetRowCount = 1; // At least one (estimated), or the for loop will never run :-)
            var targetRowCountStep = 0; // Prevents incrementing toward target row count.. unless set to 1.
            Application.Lock();
            // Clear out any meta state
            if (Application(this.NAME + "__allcached")) {
                targetRowCount = Application(this.NAME + "__totalrows");
                targetRowCountStep = 1;
                Application.Contents.Remove(this.NAME + "__allcached");
                Application.Contents.Remove(this.NAME + "__totalrows");
                Application.Contents.Remove(this.NAME + "__rawslots");
                Application.Contents.Remove(this.NAME + "__rawholes");
            }
            var tablePattern = new RegExp("^" + this.NAME); // Match the beginning of the string starting with NAME...
            // Here's some IE-only weird (but required) enumeration logic for ASP Collections (for..in doesn't work by default???)
            for (var appIterator = new Enumerator(Application.Contents), i = 0; !appIterator.atEnd() && (i < targetRowCount) ; appIterator.moveNext()) {
                var key = appIterator.item();
                // I must search the whole Application object for there is no other segregation of objects possible that persists across sessions...
                if (tablePattern.test(key)) { // This key belongs to this table...
                    Application.Contents.Remove(key);
                    i+= targetRowCountStep; // _may_ increment if the number of targetRowCount is known...
                }
            }
            // All in-memory keys removed.
            // Delete the table file itself if it exists
            var fs = Server.CreateObject("Scripting.FileSystemObject");
            if (fs.FileExists(Server.MapPath(this.NAME + FILE_EXTENSION)))
                fs.DeleteFile(Server.MapPath(this.NAME + FILE_EXTENSION));
            Application.Unlock();
        }

        
        // Get the meta information about this table (only for fully-loaded tables).
        // If no meta information is available, then an empty object is returned.
        // This method does not throw exceptions.
        // Meta structure is:
        // { totalRows: ##, fileSlotCount: ##, fileFreeSlot: ## }
        Table.prototype.getMeta = function() {
            var metaAvailable = Application(this.NAME + "__allcached");
            if (metaAvailable) {
                return {
                    totalRows: Application(this.NAME + "__totalrows"),
                    fileSlotCount: Application(this.NAME + "__rawslots"),
                    fileFreeSlot: Application(this.NAME + "__rawholes")
                };
            }
        }


        // Helper function extension to the basic types...

        // Simple handler that converts the number to a string and pads it.
        Number.prototype.pad = function(limit) {
            return this.toString().pad(limit);
        }


        Number.prototype.toUnicodeEscapeSequence = function() {
            var num = this.valueOf();
            if (num > 65535)
                throw Error("cannot convert to unicode surrogate pairs");
            var hex = [];
            while ((num / 16) > 0) {
                if ((num & 15) > 9) // Mask last 4 bits (a hex character)
                    hex.unshift(String.fromCharCode( 87 + (num & 15))); // 97 - 10 + [10-15] = a-f (unshift adds to the front of the array)
                else
                    hex.unshift(String.fromCharCode( 48 + (num & 15))); // 48 === "0"
                num = num >> 4; // Shift off the last 4 bits.
            }
            while (hex.length < 4)
                hex.unshift("0");
            return "\\u" + hex.join("");
        }

        // This function assumes that value will not exceed limit,
        // but the inverse is expected. Value is always converted to a string.
        String.prototype.pad = function(limit) {
            var diff = limit - this.length;
            if (diff == 0)
                return this; // Fast-path for fully-padded values
            var spacers = [];
            for (var i = 0; i < diff; i++)
                spacers.push(" ");
            return this + spacers.join("");
        }


        // Swaps out whatever was at startIndex with the newValue for the length of the newValue
        String.prototype.overwrite = function (newValue, startIndex) {
            return [this.slice(0, startIndex), newValue, this.slice(startIndex + newValue.length)].join("");
        }


        // Implements 'trim' for whitespace trimming...
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, '');
        }


        // Makes a string safe for embedding in another string...
        String.prototype.applyEscapeSequence = function() {
            var escaped = [];
            for (var i=0, len = this.length; i < len; i++) {
                var charCode = this.charCodeAt(i);
                if (charCode > 126)
                    escaped.push(charCode.toUnicodeEscapeSequence());
                else if ((charCode == 39) || // (') Single-quote
                         (charCode == 34) || // (") Double-quote
                         (charCode == 92)) // (\) Backslash
                    escaped.push("\\" + this.charAt(i));
                else if (charCode == 8) // (backspace) \b
                    escaped.push("\\b");
                else if (charCode == 12) // (formfeed) \f
                    escaped.push("\\f");
                else if (charCode == 10) // (newline) \n
                    escaped.push("\\n");
                else if (charCode == 13) // (carriage return) \r
                    escaped.push("\\r");
                else if (charCode == 9) // (horizontal tab) \t 
                    escaped.push("\\t");
                else if (charCode == 11) // (vertical tab) \v
                    escaped.push("\\v");
                else if (charCode == 0) // (null) \0
                    escaped.push("\\0");
                else if (charCode < 32) // Catch all for unknowns (how can these be encoded??)
                    throw Error("unescapable character code found");
                else
                    escaped.push(this.charAt(i));
            }
            return escaped.join("");
        }

        // Opens and returns a TextStream ASP object if it exists, otherwise returns null.
        function _openFlatTextFile(name, toWrite) {
            var fs = Server.CreateObject("Scripting.FileSystemObject");
            if (!toWrite && fs.FileExists(Server.MapPath(name + FILE_EXTENSION)))
                return fs.OpenTextFile(Server.MapPath(name + FILE_EXTENSION), 1/*read*/, false /*create a file if no-exist*/, -1 /*-1=Unicode,0=ASCII*/);
            else if (toWrite)
                return fs.OpenTextFile(Server.MapPath(name + FILE_EXTENSION), 2/*write*/, true /*create a file if no-exist*/, -1 /*-1=Unicode,0=ASCII*/);
            else
                return null;
        }


        // Search the text file (progressively) from the starting row, looking for
        // empty rows to use. The preferContinuation flag may make the algorithm
        // run longer while looking to fill an empty row that also belongs to a 
        // continuation chain (no more than one look-ahead guarantee).
        // Returns the rowIndex of a suitable empty starting row, or -1 if 
        // nothing was found.
        function _searchEmptyRow(allFileText, ROWSIZE, startingRow, preferContinuation) {
            var fallbackCandidate = -1;
            for (var i = INT + (startingRow * ROWSIZE), len = allFileText.length; i < len; i += ROWSIZE) {
                if (parseInt(allFileText.substr(i + INT, FLAG)) == 0) { // Found empty row...
                    if (!preferContinuation)
                        return startingRow;
                    else { // Check if this empty row has a continuation...
                        if (parseInt(allFileText.substr(i, INT)) > 0) // Bingo!
                            return startingRow;
                        else if (fallbackCandidate < 0) // the first found non-continuation empty slot is our fallback in case the optimum can't be found
                            fallbackCandidate = startingRow;
                    }
                }
                startingRow++;
            }
            // no luck!
            if (fallbackCandidate >= 0)
                return fallbackCandidate;
            return -1; // Not found.
        }


        // Marks the starting row and any continuations as free rows (for
        // future reclamation), as long as they are not already freed. 
        // Returns a structure of results:
        // * { newFile: [string], numFreed: [number] }
        function _freeRows(allFileText, ROWSIZE, startingRow) {
            var i = INT + (startingRow * ROWSIZE); // continuation pointer
            var hasContinuation = (parseInt(allFileText.substr(i + INT, FLAG)) != 0); // Read the starting flag, if it's already freed, then quick-exit.
            var numRemoved = 0;
            while (hasContinuation) {
                allFileText = allFileText.overwrite("0".pad(FLAG), i + INT);
                numRemoved++;
                var nextIndex = parseInt(allFileText.substr(i, INT));
                if (nextIndex > 0)
                    i = INT + (nextIndex * ROWSIZE);
                else
                    hasContinuation = false;
            }
            return { newFile: allFileText, numFreed: numRemoved };
        }

        
        // The hard-coded master table (tracks all created tables in the DB)
        var masterTableLayout = [
            { name: "tableName", size: 100 },
            { name: "commaSeparatedColNames", size: 400 },
            { name: "commaSeparatedColSizes", size: 50 }
        ];
        

        var JSON = {
            stringify: function(x) {
                if (typeof x == "undefined") return;
                if (x === null) return "null";
                if (typeof x == "function") return; // Skip
                if (typeof x == "string") return "\"" + x.applyEscapeSequence() + "\"";
                if (typeof x != "object") return x.toString();
                if (typeof x.length == "number") {
                    var arrayStr = "[";
                    for (var i=0;i<x.length;i++) arrayStr += ((arrayStr == "[") ? "" : ",") + JSON.stringify(x[i]);
                    return arrayStr + "]";
                }
                // object...
                if (x.___visited_)
                    throw Error("Cyclical JSON structure detected");
                x.___visited_ = true;
                var objectStr = "{";
                for (var k in x) objectStr += (k == "___visited_") ? "" : ((objectStr == "{") ? "" : ",") + JSON.stringify(k) + ":" + JSON.stringify(x[k]);
                return objectStr + "}";
            }
        };


        // Helper for Query String processing (reading the Query String via the built-in ASP 
        // objects was not working well... (e.g., ?status was reporting a count of zero...?)
        // Sets keys for all seen query string parameters. Boolean parameters are an empty 
        // object (which is 'truthy') but have no "value" sub-key.
        function _ASPQueryStringToJSObject(qs) {
            var tokens = qs.split("&");
            var newQs = {};
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i].split("=");
                newQs[decodeURIComponent(token[0].replace(/\+/g," "))] = (token.length == 1) ? true : decodeURIComponent(token[1].replace(/\+/g," "));
            }
            return newQs;
        }

        // Used to ensure that certain query string names also have a value.
        function _ensureValue(queryTerm) {
            return (typeof queryTerm == "string") ? queryTerm: null;
            // Even for query terms that existed but had the value of true, make them falsey.
        }


        // Returns the requested table layout for a given table name, or null if the table
        // name is not found in the master table.
        function _loadTableLayout(tableName) {
            if (tableName == MASTER_TABLE_NAME)
                return masterTableLayout; // Pre-configured column info...
            else { // Look in master for the row configuration options...
                var masterEntries = (new Table(MASTER_TABLE_NAME, masterTableLayout)).readAllRows();
                var rowIndex = _findRowIndex(masterEntries, 0 /* table name column*/, tableName, false);
                if (rowIndex < 0)
                    return null;
                // masterEntries[rowIndex] Array entries:
                //                 0                                     1                                           2
                // [{ name: "tableName", size: 100 },{ name: "commaSeparatedColNames", size: 400 },{ name: "commaSeparatedColSizes", size: INT }
                var otherTableColNames = masterEntries[rowIndex][1].split(",");
                var otherTableColSizes = masterEntries[rowIndex][2].split(",");
                var tableLayout = [];
                // Dynamically create the necessary table structure...
                for (var i = 0, len = otherTableColNames.length; i < len; i++) // The two arrays will be the same length (or there is data corruption)
                    tableLayout.push({ name: otherTableColNames[i], size: parseInt(otherTableColSizes[i]) });
                return tableLayout;
            }
        }


        // Helper returns the first row (or all rows) that matche the find term
        // If shouldFindAll is unset/false:
        //  * If no rows are found, returns -1
        //  * If one result is found, return the number (0 or greater)
        // If shouldFindAll is set/true:
        //  * If no results are found, returns an empty array
        //  * If one or more results are found, returns the related indexes in the array elements
        function _findRowIndex(tableResults, targetColIndex, term, shouldFindAll) {
            var findResults = [];
            for (var x in tableResults) {
                if (tableResults[x][targetColIndex] == term) {
                    if (shouldFindAll)
                        findResults.push(parseInt(x));
                    else
                        return parseInt(x);
                }
            }
            if (shouldFindAll)
                return findResults;
            else
                return -1;
        }


        // Helper that converts a table column name to it's corresponding index
        // If the name doesn't match, then returns -1.
        function _colNameToColIndex(tableColumnLayout, colName) {
            // Ensure that 'in' maps to an existing column name
            for (var i = 0; i < tableColumnLayout.length; i++) {
                if (tableColumnLayout[i].name == colName)
                    return i;
            }
            return -1; // Not found.
        }


        // Helper to return a Bad Request (400) Error message.
        function _400(message) {
            Response.Status = "400";
            Response.ContentType = "text/plain";
            Response.Write("Request Error: " + message);
            return false;
        }


        // Helper to return a JSON object success result
        function _200JSON(obj) {
            Response.Status = "200";
            Response.ContentType = "text/json";
            Response.Write(JSON.stringify(obj));
            return true;
        }
        

        // Process the Request
        function processRequest() {
            // Clients should never cache this services' responses.
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;
            var isGet = (/get/i.test(new String(Request.ServerVariables("REQUEST_METHOD")))) ? true : false;
            if (!isGet && (/multipart\/form-data/i.test(new String(Request.ServerVariables("CONTENT_TYPE"))))) {
                _400("multipart/form-data POST methods are not supported in this version");
                return;
            }
            if (!isGet && (Request.Form.Count > 0)) {
                // NOTE: Post-submitted commands in ASP are NOT automatically decoded when accessed
                // via the raw Form collection serialization (+ handling is specifically needed for 
                // auto-form-generated submissions)

                // POST request supported commands 
                // table=ABC    -- name of table to operate on (required for all operations)
                // create=...   -- create command
                //  [...] = (colName1,colSize1,colName2,colSize2,etc.) --even number of elements :) names should only be A..Za..z_ values!!
                // rmtable      -- remove the table
                // row=###      -- a row specifier (for the del/write command)
                // col=ABC      -- a column specifier (optional) if present, the column name means that only the data for the specified column is relevant to the command, otherwise all columns are assumed relevant.
                // del          -- delete a row from the table. Requires: row.
                // ins          -- insert at a specified row. Requires: row
                // add          -- add (do not replace) the row to the table
                // _ABC=...     -- the column name preceeded by an underscore
                
                //             +------------------------------------------+-------------+
                //             | REQUIRED SUBMIT DATA                     | Return data |
                // +-----------+-----------+---------+---------+----------+-------------+
                // | COMMANDS  | table=ABC | row=### | col=ABC | _ABC=ENC |             |
                // +-----------+-----------+---------+---------+----------+             |
                // | create=.. | Y         |         |         |          | void        |
                // | rmtable   | Y         |         |         |          | void        |
                // | del       | Y         | Y       |         |          | void        |
                // | ins       | Y         | Y       | (Y)     | Y        | void        |
                // | add       | Y         |         |         | Y        | row index   | { addedIndex: ## }
                // +-----------+-----------+---------+---------+----------+-------------+
                var data = _ASPQueryStringToJSObject(new String(Request.Form));
                data.table = _ensureValue(data.table);
                data.row = _ensureValue(data.row);
                data.col = _ensureValue(data.col);
                data.create = _ensureValue(data.create);
                // DEBUG------------------------------
                if (data.clearcache) {
                    Application.Lock();
                    Application.Contents.RemoveAll();
                    Application.Unlock();
                    return;
                }
                // DEBUG------------------------------
                var commandCount = 0;
                if (data.create)
                    commandCount++;
                if (data.rmtable)
                    commandCount++;
                if (data.del)
                    commandCount++;
                if (data.ins)
                    commandCount++;
                if (data.add)
                    commandCount++;
                if (commandCount != 1)
                    return _400("missing or too many commands specified");
                if (data.table == MASTER_TABLE_NAME)
                    return _400("table operation failed (master index is reserved/cannot be modified directly)");
                var queryTableLayout = _loadTableLayout(data.table);
                if (!queryTableLayout && !data.create) // If the table name doesn't exist, and you're not going to create it...
                    return _400("table name not found (does not exist [yet])");
                var masterTable = null;
                if (data.create) {
                    if (queryTableLayout) // I actually found a table
                        return _400("table creation failed (table name already exists)");
                    var invalidCharsRegexp = /[^A-Za-z_]/g; // Everything outside of this range.
                    if (invalidCharsRegexp.test(data.table))
                        return _400("table creation failed (invalid character found in table name; A-Z, a-z, and underscore only)");
                    // Table name doesn't already exist...add it to the master table
                    var futureTableNameAndSizeArray = data.create.split(",");
                    if (futureTableNameAndSizeArray.length % 2) // Even numbers mod to 0 (false), so "if not even"...
                        return _400("table creation failed (missing equal number of name, size pairs)");
                    // Sanitize the input...
                    var colNames = [];
                    var colSizes = [];
                    for (var i = 0; i < futureTableNameAndSizeArray.length; i++) {
                        var tempColItem = futureTableNameAndSizeArray[i].trim();
                        if (i % 2) { // odd numbers (expected size entries)
                            tempColItem = parseInt(tempColItem);
                            if (!isFinite(tempColItem) || isNaN(tempColItem) || (tempColItem <= 0))
                                return _400("table creation failed (invalid column size found; size must be at least 1)");
                            colSizes.push(tempColItem);
                        }
                        else { // even numbers (expected name entries)
                            // Make sure name restrictions applies
                            if (invalidCharsRegexp.test(tempColItem))
                                return _400("table creation failed (invalid character found in column name; A-Z, a-z, and underscore only)");
                            colNames.push(tempColItem);
                        }
                    }
                    // Write the data to Master...
                    masterTable = new Table(MASTER_TABLE_NAME, _loadTableLayout(MASTER_TABLE_NAME));
                    masterTable.writeRow(-1, [data.table, colNames.join(","), colSizes.join(",")]);
                    return; // 200 will be returned (void response)
                }
                // Table found (from potentially untrusted data, so take care in opening it...)
                try { var table = new Table(data.table, queryTableLayout); }
                catch (err) {
                    return _400(err.message);
                }
                if (data.rmtable) {
                    masterTable = new Table(MASTER_TABLE_NAME, _loadTableLayout(MASTER_TABLE_NAME));
                    var allRows = masterTable.readAllRows();
                    var rowIndex = _findRowIndex(allRows, 0 /* table name column*/, data.table, false);
                    if (rowIndex < 0)
                        return _400("table removal failed (table not found)");
                    masterTable.deleteRow(rowIndex);
                    table.deleteAllRows();
                    return; // 200 will be returned (void response).
                }
                // Valid the row data, if provided
                var row = null;
                if (data.del || data.ins) {
                    if (data.row == null) // Falsy values like 0 are allowed...
                        return _400("table row deletion failed (row quantifier not provided)");
                    row = parseInt(data.row);
                    if (isNaN(row) || !isFinite(row) || (row < 0))
                        return _400("table row deletion failed (row quantifier invalid value)");
                    // Row is good so far...
                }
                if (data.del) {
                    try { 
                        table.deleteRow(row); 
                        return; // 200 will be returned (void response)
                    }
                    catch (err) {
                        return _400(err.message);
                    }
                }
                // Valid the col data, if provided
                var col = -1; // Assume no column info...
                if (data.col) {
                    col = _colNameToColIndex(queryTableLayout, data.col);
                    if (col < 0) // If column info was provided, it's an error if it doesn't match anything
                        return _400("table row insertion failed (column name specifier not found)");
                    // Col index is ready
                }
                // Collect the data to insert/add & validate 
                var colData = [];
                for (var i = 0; i < queryTableLayout.length; i++) {
                    colData.push(_ensureValue(data["_" + queryTableLayout[i].name]));
                    // Recall that _ensureValue returns null for non-valued data...
                    // If I should collect all columns, OR I'm looking for a specific column, and this is it, then if I got a null value from ensureValue...
                    if (((col < 0) || ((col >= 0) && (i == col))) && !colData[i])
                        return _400("table row insertion/add failed (missing column \""+queryTableLayout[i].name+"\" data; prefix with an underscore)");
                }
                if (data.add) {
                    try {
                        return _200JSON( { addedIndex: table.writeRow(-1, colData) } );
                    }
                    catch (err) {
                        return _400(err.message);
                    }
                }
                if (data.ins) {
                    try {
                        if (col < 0) // Insert a whole row
                            table.writeRow(row, colData);
                        else {
                            // First read the row, then write the row back with modified column
                            var rowData = table.readRow(row)[row];
                            rowData[col] = colData[col];
                            table.writeRow(row, rowData);
                        }
                        return; // 200 will be returned (void response)
                    }
                    catch (err) {
                        return _400(err.message);
                    }
                }
                return _400("POST command not yet supported"); // This won't happen unless I introduce new commands but forget to handle them in the above flow.
            }
            else if (isGet && (Request.QueryString.Count > 0)) { // GET request w/Query String
                // Query commands supported (in order of precedence high-to-low):
                // table=ABC    -- name of table to get
                // status       -- request to display status (no value required--default value when absent is no-status)
                // row=###|all  -- request for a specific row (default value if omitted [and status set] is "all", meaning all rows)
                // col=ABC      -- filter results to the named column in the table (if omitted assumes all the columns in the row(s)
                // is=ABC       -- filter row results to those where the column (see 'in') matches ABC (case sensitive). If 'in' not specified then fail the request
                // in=ABC       -- the column to use with the 'is' filter.
                // Results are in JSON form, where the row identifier is returned as the key in a dictionary of results, where the column results are an array.
                var qs = _ASPQueryStringToJSObject(new String(Request.QueryString)); // Coerce QueryString to a string (its a collection by default)
                qs.table = _ensureValue(qs.table);
                qs.row = _ensureValue(qs.row);
                qs.col = _ensureValue(qs.col);
                qs.is = _ensureValue(qs.is);
                qs._in = _ensureValue(qs["in"]);
                // Now for the commands that require a value they were null-ified if a value was not provided.
                // Must have a table name
                if (!qs.table) // Cannot be a boolean parameter
                    return _400("table unspecified");
                // There is a qs.table
                if (qs.status && (qs.row == null)) // It's null and status is requested...
                    qs.row = "all";
                if ((qs.row != null) && (qs.row != "all")) // If there's a row but the value isn't 'all'
                    qs.row = parseInt(qs.row);
                if (qs.row == null) // At this point, it should be set, or this is a no-go query...
                    return _400("row unspecified (did you mean to ask for status?)");
                // Rows are now set...
                // Additional QueryString validation (that can be done before looking up any data...
                if (qs.is && !qs._in) // If 'is' is present but not 'in'...
                    return _400("'is' query term specified without 'in' required column name");
                var queryTableLayout = _loadTableLayout(qs.table);
                if (queryTableLayout == null)
                    return _400("table name not found (does not exist [yet])");
                var table = new Table(qs.table, queryTableLayout);
                // Do the read
                var readResult = null;
                try {
                    if (qs.row == "all")
                        readResult = table.readAllRows();
                    else
                        readResult = table.readRow(qs.row);
                    // Filter results? -- is + in requirement pre-validated.
                    if (qs.is) {
                        // Ensure that 'in' maps to an existing column name
                        qs._in = _colNameToColIndex(queryTableLayout, qs._in);
                        if (qs._in < 0) // Not found...
                            return _400("'in' column name not found in table");
                        // Filter the readResult for the is search term...
                        for (var x in readResult) {
                            if (readResult[x][qs._in] != qs.is)
                                delete readResult[x]; // Remove the non-matching key...
                        }
                        // Filtered...
                    }
                    if (qs.col) { // Column filtering is desired
                        // Column filtering simplifies the result structure. Normally each rowIndex key is an array of column values, but with filtering active
                        // only one column will be returned, so the array containing structure is dropped and each key's value is only the column result string itself...
                        qs.col = _colNameToColIndex(queryTableLayout, qs.col);
                        if (qs.col < 0)
                            _400("'col' column name not found in table");
                        // Reduce each row to the single column value.
                        for (var x in readResult) {
                            readResult[x] = readResult[x][qs.col];
                        }
                        // Column-scoped...
                    }
                    if (qs.status) { // I previously read all the rows--asking for meta information is safe.
                        var meta = table.getMeta(); // May return an object with no meta info!
                        for (var x in meta) // If there are no results, this for loop fails out fast.
                            readResult[x] = meta[x];
                    }
                    return _200JSON(readResult);
                }
                catch (err) {
                    _400(err.message);
                }
            }
            else {
                // Request sans any post/query string -- give a generic server status
                Response.Write("\
                  <!DOCTYPE html>\
                  <html><head><title>Simple Table Service Status</title>\
                    <style>\
                      html { height: 100%; font-family: \"Segoe UI\"; background-color: #053501; background-image: radial-gradient(at 20% 20%, #17CE02 0%, #053501 80%, #053501 100%); }\
                      h1 { font-size: 100pt; text-shadow: 5px 5px 20px 20px #17CE02; margin-top: -0.2em; margin-bottom: -0.3em; }\
                      .bottom { position: absolute; padding: 8px; bottom: 0; right: 0; }\
                    </style>\
                  </head>\
                  <body>\
                   <h1>STS</h1>\
                   <h4>Simple Table Service Server v.1.0 (Classic ASP)</h4>\
                   <h6 class=\"bottom\">by Travis Leithead, 2013</h6>\
                  </body>\
                 </html>");
            }
        }
        
        processRequest();

    %>